
// Playing Cards
// James Michalski

#include <iostream>
#include <conio.h>
#include <string> 

using namespace std;

enum Suit {Spades, Clubs, Hearts, Diamonds};

enum Rank {Two = 2, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace};

struct Card
{ Rank Rank; Suit Suit; };


int main()
{
	(void)_getch();
	return 0;
}
